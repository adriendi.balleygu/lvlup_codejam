﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTriggerScript : MonoBehaviour
{
    public Animator animator;
    public GameObject manager;

    private bool trig = false;
    int c = 0;

    void OnTriggerEnter2D(Collider2D collider){
        animator.SetTrigger("BathroomAnim");
        trig = true;
    }

    void FixedUpdate(){
        if(trig){
            if(++c >= 190){
                manager.GetComponent<HouseManager>().SetLevelOver();
                trig = false;
            }
        }
    }
}
