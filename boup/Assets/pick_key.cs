﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pick_key : PickableScript
{
    public Level level;
    public int indexOfRoomToOpen;
    public bool openLeftSide;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(waitToStart(2.0f));
    }

    IEnumerator waitToStart(float sec) {
        yield return new WaitForSeconds(sec);
        if (openLeftSide) level.GetComponent<Level>().setTransitionLeftActive(indexOfRoomToOpen, false);
        else level.GetComponent<Level>().setTransitionRightActive(indexOfRoomToOpen, false);
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }

    public override void pickedEvent() {
        Debug.Log("boup");
        if (openLeftSide) level.GetComponent<Level>().setTransitionLeftActive(indexOfRoomToOpen, true);
        else level.GetComponent<Level>().setTransitionRightActive(indexOfRoomToOpen, true);
    }
}
