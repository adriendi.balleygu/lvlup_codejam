﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableScript : MonoBehaviour
{
    public GameObject dialogManager;
    public GameObject player;
    public string onPickSpeech;
    private bool isPickable;

    // Start is called before the first frame update
    void Start()
    {
        isPickable = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D collider){
        if(collider == player.GetComponentInChildren<Collider2D>()){
            //Debug.Log("Object is pickable");
            isPickable = true;
            player.GetComponentInChildren<InteractScript>().SetPickable(transform.parent.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D collider){
        if(collider == player.GetComponentInChildren<Collider2D>()){
            //Debug.Log("Object is not pickable");
            isPickable = false;
        }
    }

    public bool HasSpeech(){
        return onPickSpeech != "";
    }

    public void MakeSpeech(){
        dialogManager.GetComponent<DialogTextManager>().MakeTextBubble(onPickSpeech);
    }

    public bool GetIsPickable(){
        return isPickable;
    }

    public virtual void pickedEvent() { 
        
    }
}
