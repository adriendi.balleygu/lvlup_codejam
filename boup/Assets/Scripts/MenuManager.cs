﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    public Transform UIPanel; // to enable/disable pause panel

    public GameObject player;

    bool isPaused; // to define pause state

    // Start is called before the first frame update
    void Start()
    {
        UIPanel.gameObject.SetActive(false); // pause menu disabled at start
        isPaused = false; // define isPaused to false at start
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !isPaused)
        {
            Pause();
        }
            
        else if (Input.GetKeyDown(KeyCode.Escape) && isPaused)
        {
            UnPause();
        }
    }

    public void Pause()
    {
        isPaused = true;
        UIPanel.gameObject.SetActive(true); // show pause menu
        player.GetComponentInChildren<PlayerMovement>().enabled = false;
    }

    public void UnPause()
    {
        isPaused = false;
        UIPanel.gameObject.SetActive(false); // hide pause menu
        player.GetComponentInChildren<PlayerMovement>().enabled = true;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Restart()
    {
        SceneManager.LoadScene("MenuScene", LoadSceneMode.Single);
    }

}
