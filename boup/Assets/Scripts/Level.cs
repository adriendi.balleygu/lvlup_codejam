﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Level : MonoBehaviour
{
    public AudioClip music;
    public GameObject[] roomList;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public abstract void init_level();

    public AudioClip getMusic() {
        return music;
    }

    public GameObject[] getRoomList()
    {
        return roomList;
    }

    public void setTransitionLeft(int origin, int dest)
    {
        roomList[origin].transform.GetChild(0).gameObject.GetComponent<DoorScript>().SetCameraDestination(roomList[dest]);
        roomList[origin].transform.GetChild(0).gameObject.GetComponent<DoorScript>().SetDestination(roomList[dest].transform.GetChild(1).gameObject.transform.position);
    }

    public void setTransitionRight(int origin, int dest)
    {
        roomList[origin].transform.GetChild(1).gameObject.GetComponent<DoorScript>().SetCameraDestination(roomList[dest]);
        roomList[origin].transform.GetChild(1).gameObject.GetComponent<DoorScript>().SetDestination(roomList[dest].transform.GetChild(0).gameObject.transform.position);
    }

    public void setTransitionRightActive(int room, bool newState)
    {
        roomList[room].transform.GetChild(1).gameObject.GetComponent<DoorScript>().setActive(newState);
    }

    public void setTransitionLeftActive(int room, bool newState)
    {
        roomList[room].transform.GetChild(0).gameObject.GetComponent<DoorScript>().setActive(newState);
    }

}
