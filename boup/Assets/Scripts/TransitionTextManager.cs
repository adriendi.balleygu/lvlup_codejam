﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransitionTextManager : MonoBehaviour
{
    
    [SerializeField]
    public Transform UIPanel;
    [SerializeField]
    public Text introText;
    public Text titleText;
    public Text inputPrompt;

    private int counter=0;
    private int counterTitle=0;
    private int currentLevel=0;

    private string[] titleLvl = {
        "Chapter 1: Oh happy day",
        "Chapter 2: It's not going very well",
        "Chapter 3: Worst nightmare ever"
    };

    private string[] introLvl = {
        "It’s ten in the morning. Lily is waking up. Today is her birthday and she is looking forward to seeing her friends, playing with her cat and hugging her parents. \n\nShe also knows she’ll receive a nice gift. \n\nBut before all this, she needs to prepare herself and then find everyone.",
        "It’s eleven in the morning. Lily is waking up. Today is her birthday and she is looking forward to seeing her best friend, playing with her cat and seeing her parents. \n\nShe also knows she’ll receive a nice gift. \n\nBut before all this, she needs to prepare herself and then… find everyone.",
        "It’s noon. Lily is waking up. Today is her birthday and she is looking forward to playing with her cat and seeing her parents. \n\nShe also knows she’ll probably receive a gift. \n\nBut before all this, she needs to prepare herself and then… find someone."
    };

    private int mod=5;
    private int modTitle=5;
    private bool textHasAppeared=false;
    private string currentText;
    private string currentTitle;
    
    // Start is called before the first frame update
    void Start()
    {
        inputPrompt.gameObject.SetActive(false);
        currentText = introLvl[0];
        currentTitle = titleLvl[0];
    }

    // Update is called once per frame
    void Update()
    {
        if(!textHasAppeared){            
            ShowTitle();
            ShowText();
            if(Input.GetKeyDown(KeyCode.Space))
                ShowWholeText();
        } 
    }

    private void ShowWholeText(){
        titleText.text = currentTitle;
        introText.text = currentText;
        textHasAppeared= true;
        inputPrompt.gameObject.SetActive(true);
    }

    private void ShowTitle(){
        if(counterTitle < currentTitle.Length){
            if(--modTitle <= 0){
                titleText.text += currentTitle[counterTitle++];
                modTitle = 5;
            }
        }
    }

    private void ShowText(){
        if(counter < currentText.Length){
            if(--mod <= 0){
                //Debug.Log(currentText[counter]);
                introText.text += currentText[counter++];
                mod = 5;
            }
        }else{
            textHasAppeared = true;
            inputPrompt.gameObject.SetActive(true);
        }
    }

    public void Hide(){
        UIPanel.gameObject.SetActive(false);
    }

    public void ShowNewLevel(){
        Debug.Log("Showing new level");
        inputPrompt.gameObject.SetActive(false);
        UIPanel.gameObject.SetActive(true);
        counter = 0;
        counterTitle = 0;
        if(currentLevel < introLvl.Length-1){
            currentLevel++;
        }
        currentText = introLvl[currentLevel];
        currentTitle = titleLvl[currentLevel];
        mod = 5;
        modTitle = 5;
        introText.text = "";
        titleText.text = "";
        textHasAppeared = false;
    }

    public bool GetHasAppeared(){
        return textHasAppeared;
    }
}
