﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    public GameObject player;
    public GameObject gameCamera;
    public GameObject houseManager;
    public bool isLeftDoor;
    private float xOffset = -1.5f;
    private float yOffset = 1.0f;
    private Vector3 playerDestination;

    private GameObject cameraDestination;

    private int roomRotation;

    //spawn point in the next room
    public void SetDestination(Vector3 newDest){
        playerDestination = newDest;
    }

    //room position to move the camera there
    public void SetCameraDestination(GameObject room){
        cameraDestination = room;
        int rotation = cameraDestination.GetComponent<RoomGravityHandler>().rotation;
    }

    // Start is called before the first frame update
    void Start()
    {
        if(!isLeftDoor){
            xOffset = -xOffset;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //called when something enters the boxcollider2d
    void OnTriggerEnter2D(Collider2D collider){
        if(collider == player.GetComponentInChildren<Collider2D>()){
            gameCamera.GetComponent<CameraFade>().SetQuickTrigger(true, this);
        }
    }

    //callback method to tp player only when the fade out is over
    public void TeleportToRoom(){ 
        player.transform.position = playerDestination + new Vector3(xOffset,yOffset,0);
        houseManager.GetComponent<HouseManager>().SetCurrentRoom(cameraDestination);
        gameCamera.transform.position = new Vector3(cameraDestination.transform.position.x, cameraDestination.transform.position.y, -100);
    }

    public void RotationAdjustement(int rotation){
        roomRotation = rotation;
    }

    public void setActive(bool newState)
    {
        this.GetComponent<CapsuleCollider2D>().enabled = newState;
    }
}
