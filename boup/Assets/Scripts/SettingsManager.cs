﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SettingsManager : MonoBehaviour
{
    readonly string[] RESOLLIST = { "800x600", "1024x768", "1280x720", "1366x768", "1920x1080" };
    const int DEFAULTRESOLINDEX = 4;

    public Toggle fullScreenToggle;

    // Start is called before the first frame update
    void Start()
    {
        int currentResolIndex = PlayerPrefs.GetInt("ScreenResolution", DEFAULTRESOLINDEX);
        DefineResolution(currentResolIndex);

        // to check that the toggle is correctly set
        if(Convert.ToBoolean(PlayerPrefs.GetString("FullScreen", "true")))
        {
            fullScreenToggle.isOn = true;
            Screen.fullScreen = true;
        }
        else
        {
            fullScreenToggle.isOn = false;
            Screen.fullScreen = false;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    //TODO There is an issue
    // When toggle, ok, change resolution ok, windowed and exit, then run, ok, change resolution not ok ! -> full
    // 
    public void FullScreenToggle()
    {
        if (Screen.fullScreen)
        {
            PlayerPrefs.SetString("FullScreen", "false");
            Screen.fullScreen = false;
        }
        else
        {
            PlayerPrefs.SetString("FullScreen", "true");
            Screen.fullScreen = true;
        }
    }

    public void ResolutionButton()
    {
        int currentResolIndex = PlayerPrefs.GetInt("ScreenResolution", DEFAULTRESOLINDEX);
        DefineResolution((currentResolIndex+1)%(DEFAULTRESOLINDEX+1));
    }

    public void BackButton()
    {
        SceneManager.LoadScene("MenuScene", LoadSceneMode.Single);
    }

    /**
     * Function to define the screen resolution in function of an index given as parameter
     * Fullscreen is defined through player's preferences, default is true.
     * Screenresolution hardcoded and defined through index in array RESOLLIST ["800x600","1024x768","1280x720","1366x768","1920x1080"]
     * Index correspond to next resolution to display on ResolutionButton, not current screen resolution.
     */
    private void DefineResolution(int index)
    {
        bool fullScreenBool = Convert.ToBoolean(PlayerPrefs.GetString("FullScreen", "true"));
        Debug.Log(index);
        switch (index)
        {
            case 0:
                GameObject.Find("ResolutionButton").GetComponentInChildren<Text>().text = RESOLLIST[index + 1];
                Screen.SetResolution(800, 600, fullScreenBool);
                break;
            case 1:
                GameObject.Find("ResolutionButton").GetComponentInChildren<Text>().text = RESOLLIST[index + 1];
                Screen.SetResolution(1024, 768, fullScreenBool);
                break;
            case 2:
                GameObject.Find("ResolutionButton").GetComponentInChildren<Text>().text = RESOLLIST[index + 1];
                Screen.SetResolution(1280, 720, fullScreenBool);
                break;
            case 3:
                GameObject.Find("ResolutionButton").GetComponentInChildren<Text>().text = RESOLLIST[index + 1];
                Screen.SetResolution(1366, 768, fullScreenBool);
                break;
            case 4:
                GameObject.Find("ResolutionButton").GetComponentInChildren<Text>().text = RESOLLIST[0];
                Screen.SetResolution(1920, 1080, fullScreenBool);
                break;
            default:
                Debug.Log("Ouuups, error with resolution! Shouldn't occure...");
                index = DEFAULTRESOLINDEX;
                break;
        }
        PlayerPrefs.SetInt("ScreenResolution", index);
    }
}
