﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_3: Level
{
    // Start is called before the first frame update
    void Start()
    {
        init_level();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void init_level() 
    {
        //set teleports between rooms
        //0_right to 1_left - lily to bath1
        setTransitionRight(0, 1);
        //1_right to 3_left - bath1 to kitchen
        setTransitionRight(1, 3);
        //3_right to 7_left - kitchen to basement
        setTransitionRight(3, 7);
        //7_right to 2_left - basement to parent
        setTransitionRight(7, 2);
        //2_right to 6_left - parent to attic
        setTransitionRight(2, 6);
        //6_right to 7_left - attic to guest
        setTransitionRight(6, 5);
        //5_right to 4_left - guest to living
        setTransitionRight(5, 4);
        //4_right to 0_left - living to lily
        setTransitionRight(4, 0);
        //8_right to 9_left - bath2 to hallway
        setTransitionRight(8, 9);
        //9_right to 10_left - hallway to brother
        setTransitionRight(9, 10);
        //1_left to 5_right - bath1 to guest
        setTransitionLeft(1, 5);
        //5_left to 2_right - guest to parent
        setTransitionLeft(5, 2);
        //6_left to 7_right - attic to basement
        setTransitionLeft(6, 7);
        //7_left to 3_right - basement to kitchen
        setTransitionLeft(7, 3);
        //3_left to 8_right - kitchen to bath2
        setTransitionLeft(3, 8);
        //8_left to 0_right - bath2 to lily
        setTransitionLeft(8, 0);
        //4_left to 8_right - living to bath2
        setTransitionLeft(4, 8);

        //deactivate the selected doors
        //0_left - lily left
        setTransitionLeftActive(0, false);
        //2_left - parent left
        setTransitionLeftActive(2, false);
        //9_left - hallway left
        setTransitionLeftActive(9, false);
        //10_left - brother left
        setTransitionLeftActive(10, false);
        //10_right - brother right
        setTransitionRightActive(10, false);
    }
}
