﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public GameObject dialogManager;

    public CharacterController2D controller2D;

    public Animator animator;

    public float runSpeed = 40f;
    
    float horizontalMove = 0f;
    int jumpCount = 1;
    public bool canJump = true;
    bool jump = false;
    bool hasMoved = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed; //negative when running to the left
        if(!hasMoved){
            if(horizontalMove != 0){
                hasMoved = true;
                dialogManager.GetComponent<DialogTextManager>().ShowFirstBubble(this.gameObject);
            }
        }
        
        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if(Input.GetButtonDown("Jump") && jumpCount > 0){
            jump = true;
            canJump = false;
            animator.ResetTrigger("IsJumping");
            animator.SetTrigger("IsJumping");
        }
    }

    void FixedUpdate(){
        controller2D.Move(horizontalMove * Time.deltaTime, false, jump);
        jump = false;
    }

    void OnCollisionEnter2D(Collision2D other){
        if(other.gameObject.tag == "Ground")
        {
            canJump = true;
        }
    }
}
