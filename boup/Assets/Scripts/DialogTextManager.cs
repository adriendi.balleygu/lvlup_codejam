﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogTextManager : MonoBehaviour
{
    public Transform UIPanel;
    public Text bubbleText;

    GameObject player;
    private int counter = 0;
    private int currentLevel = 0;
    private bool playerHasMoved = false;
    private int timeOnScreen = 0;

    private string[] firstBubbleAtLvl = {
        "Today's my birthday!!!",
        "I hope today is going to be better than yesterday...",
        "Where has gone the light? I’m so scared of the darkness... I want my parents back... what if She comes back? I can’t fight against Her. She is too strong."
    };

    private int mod = 5;
    private bool textHasAppeared =  false;
    private string currentText;
    private bool newBubble = false;

    // Start is called before the first frame update
    void Start()
    {
        currentText = firstBubbleAtLvl[currentLevel];
        UIPanel.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(UIPanel.gameObject.activeSelf){
            UIPanel.position = new Vector3(player.transform.position.x, player.transform.position.y + 1, player.transform.position.z);
        }

        if(playerHasMoved){
            UIPanel.gameObject.SetActive(true);
            ShowText();
        }

        if(newBubble){
            ShowText();
        }
    }

    void FixedUpdate(){
        if(textHasAppeared){
            timeOnScreen++;
            if(timeOnScreen >= 200){
                UIPanel.gameObject.SetActive(false);
            }
        }
    }

    private void ShowText(){
        if(counter < currentText.Length){
            if(--mod <= 0){
                bubbleText.text += currentText[counter++];
                mod = 5;
            }
        }else{
            textHasAppeared = true;
            playerHasMoved = false;
            newBubble = false;
        }
    }


    public void MakeTextBubble(string text){
        UIPanel.gameObject.SetActive(true);
        currentText = text;
        bubbleText.text = "";
        mod = 5;
        counter = 0;
        timeOnScreen = 0;
        textHasAppeared = false;
        newBubble = true;
    }

    public void ShowFirstBubble(GameObject player){
        this.player = player;
        playerHasMoved = true;
    }
}
