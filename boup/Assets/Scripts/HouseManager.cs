﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseManager : MonoBehaviour
{
    public GameObject transitionTextManager;
    public Level[] levels;
    public GameObject player;
    Level currentLevel;
    GameObject currentRoom;
    GameObject[] roomList;
    private bool isLevelOver = false;
    int currentLevelInt = 0;
    public GameObject gameCamera;

    int currentRotation = 0;
    private bool isIntro = true;
    Vector2 gravity;
    TransitionTextManager transitionScript;

    // Start is called before the first frame update
    void Start()
    {
        gravity = Physics2D.gravity;
        loadLevel(currentLevelInt);
        player.GetComponentInChildren<PlayerMovement>().enabled = false;
        transitionScript = transitionTextManager.GetComponent<TransitionTextManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(isIntro){
            if(Input.GetKeyDown(KeyCode.Space)){
                if(transitionScript.GetHasAppeared()){
                    isIntro = false;
                    isLevelOver = false;
                    player.GetComponentInChildren<PlayerMovement>().enabled = true;
                    transitionScript.Hide();
                }
            }
        }

        if(isLevelOver && !isIntro){
            transitionScript.ShowNewLevel();
            loadLevel(++currentLevelInt);
            isIntro = true;
            player.GetComponentInChildren<PlayerMovement>().enabled = false;
        }

    }

    public void SetLevelOver(){
        isLevelOver = true;
    }

    void RotateRoom(GameObject room, int value)
    {
        room.transform.Rotate(0, 0, value);
        room.transform.GetChild(0).gameObject.GetComponent<DoorScript>().RotationAdjustement(value);

    }

    public void SetCurrentRoom(GameObject newRoom)
    {
        currentRoom = newRoom;
        gameCamera.transform.rotation = Quaternion.Euler(0, 0, currentRoom.GetComponent<RoomGravityHandler>().rotation);
    }

    private void setAndPlayMusicFromLevel(int lvl) {
        gameCamera.GetComponent<AudioSource>().Stop();
        gameCamera.GetComponent<AudioSource>().clip = levels[lvl].getMusic();
        gameCamera.GetComponent<AudioSource>().Play();
    }

    private void loadLevel(int lvl) {
        currentLevel = levels[lvl];
        roomList = currentLevel.gameObject.GetComponent<Level>().getRoomList();
        currentRoom = roomList[0];

        currentRoom.transform.GetChild(0).gameObject.GetComponent<DoorScript>().SetCameraDestination(currentRoom);
        currentRoom.transform.GetChild(0).gameObject.GetComponent<DoorScript>().SetDestination(currentRoom.transform.position);
        gameCamera.GetComponent<CameraFade>().SetQuickTrigger(true,currentRoom.transform.GetChild(0).gameObject.GetComponent<DoorScript>());

        setAndPlayMusicFromLevel(lvl);
    }

}
