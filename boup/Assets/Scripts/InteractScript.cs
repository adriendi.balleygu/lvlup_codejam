﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractScript : MonoBehaviour
{
    GameObject pickable;

    //public GameObject[] inventory;
    public List<GameObject> inventory;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Interact")){
            if(pickable != null){
                if(pickable.GetComponentInChildren<PickableScript>().GetIsPickable()){
                    Debug.Log("Pick " + pickable.ToString());
                    inventory.Add(pickable);
                    pickable.transform.GetChild(0).GetComponent<PickableScript>().pickedEvent();
                    pickable.SetActive(false);
                    if(pickable.GetComponentInChildren<PickableScript>().HasSpeech())
                        pickable.GetComponentInChildren<PickableScript>().MakeSpeech();
                }
            }
        }
    }

    public void SetPickable(GameObject obj){
        this.pickable = obj;
    }
}
