﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_1 : Level
{

    // Start is called before the first frame update
    void Start()
    {
        init_level();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void init_level()
    {
        //set teleports between rooms
        //0_right to 1_left - lily to bath1
        setTransitionRight(0, 1);
        //1_left to 0_right - bath1 to lily
        setTransitionLeft(1, 0);
        //1_right to 2_left - bath1 to parent
        setTransitionRight(1, 2);
        //2_left to 1_right - parent to bath1
        setTransitionLeft(2, 1);
        //2_right to 3_left - parent to kitchen
        setTransitionRight(2, 3);
        //3_left to 2_right - kitchen to parent
        setTransitionLeft(3, 2);
        //3_right to 4_left - kitchen to living
        setTransitionRight(3, 4);
        //4_left to 3_right - living to kitchen
        setTransitionLeft(4, 3);
        //4_right to 5_left - living to guest
        setTransitionRight(4, 5);
        //5_left to 4_right - guest to living
        setTransitionLeft(5, 4);
        //5_right to 6_left - guest to attic
        setTransitionRight(5, 6);
        //6_left to 5_right - attic to guest
        setTransitionLeft(6, 5);
        //6_right to 7_left - attic to basement
        setTransitionRight(6, 7);
        //7_left to 6_right - basement to attic
        setTransitionLeft(7, 6);
        //7_right to 8_left - basement to bath2
        setTransitionRight(7, 8);
        

        //deactivate the selected doors
        //0_left - lily left
        setTransitionLeftActive(0, false);
        //8_left - bath2 left
        setTransitionLeftActive(8, false);
        //8_right - bath2 right
        setTransitionRightActive(8, false);
        //9_left - hallway left
        setTransitionLeftActive(9, false);
        //10_left - brother left
        setTransitionLeftActive(10, false);
        //10_right - brother right
        setTransitionRightActive(10, false);
    }

}
