﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartMenuManager : MonoBehaviour
{
    public GameObject startButton;
    public GameObject settingsButton;
    public GameObject exitButton;

    public Sprite startExitSprite;
    public Sprite startEnterSprite;
    public Sprite startClickedSprite;

    public Sprite settingsExitSprite;
    public Sprite settingsEnterSprite;
    public Sprite settingsClickedSprite;

    public Sprite exitExitSprite;
    public Sprite exitEnterSprite;
    public Sprite exitClickedSprite;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void BeginGame()
    {
        
    }

    public void QuitGame()
    {
        
    }

    public void GoToSettings()
    {
        
    }

    // start
    public void OnStartButtonClick()
    {
        startButton.GetComponent<Image>().sprite = startClickedSprite;
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
    }

    public void OnStartButtonExit()
    {
        startButton.GetComponent<Image>().sprite = startExitSprite;
    }

    public void OnStartButtonEnter()
    {
        startButton.GetComponent<Image>().sprite = startEnterSprite;
    }

    // settings
    public void OnSettingsButtonClick()
    {
        settingsButton.GetComponent<Image>().sprite = settingsClickedSprite;
        SceneManager.LoadScene("SettingsScene", LoadSceneMode.Single);
    }

    public void OnSettingsButtonExit()
    {
        settingsButton.GetComponent<Image>().sprite = settingsExitSprite;
    }

    public void OnSettingsButtonEnter()
    {
        settingsButton.GetComponent<Image>().sprite = settingsEnterSprite;
    }

    // exit
    public void OnExitButtonClick()
    {
        exitButton.GetComponent<Image>().sprite = exitClickedSprite;
        Application.Quit();
    }

    public void OnExitButtonExit()
    {
        exitButton.GetComponent<Image>().sprite = exitExitSprite;
    }

    public void OnExitButtonEnter()
    {
        exitButton.GetComponent<Image>().sprite = exitEnterSprite;
    }
}
