﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDialog : MonoBehaviour
{
    public GameObject dialogManager;
    public string textAtThisSpot;
    public GameObject player;

    void OnTriggerEnter2D(Collider2D collider){
        if(collider == player.GetComponentInChildren<Collider2D>()){
            dialogManager.GetComponent<DialogTextManager>().MakeTextBubble(textAtThisSpot);
        }
    }
}
