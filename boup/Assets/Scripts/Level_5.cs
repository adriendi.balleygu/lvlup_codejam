﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_5 : Level
{
    // Start is called before the first frame update
    void Start()
    {
        init_level();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void init_level() {
        //set teleports between rooms
        //0_right to 1_left - lily to bath1
        setTransitionRight(0, 1);
        //1_right to 0_left - bath1 to lily
        setTransitionRight(1, 0);
        //2_right to 7_left - parent to basement
        setTransitionRight(2, 7);
        //7_right to 3_left - basement to kitchen
        setTransitionRight(7, 3);
        //3_right to 2_left - kitchen to parent
        setTransitionRight(3, 2);
        //6_right to 5_left - attic to guest
        setTransitionRight(6, 5);
        //5_right to 1_left - guest to bath1
        setTransitionRight(5, 1);
        //4_right to 0_left - living to lily
        setTransitionRight(4, 0);
        //8_right to 10_left - bath2 to brother
        setTransitionRight(8, 10);

        //1_left to 2_right - bath to parent
        setTransitionLeft(1, 2);
        //2_left to 6_right - parent to attic
        setTransitionLeft(2, 6);
        //6_left to 3_right - attic to kitchen
        setTransitionLeft(6, 3);
        //3_left to 7_right - kitchen to basement
        setTransitionLeft(3, 7);
        //7_left to 1_right - basement to bath1
        setTransitionLeft(7, 1);
        //4_left to 5_right - living to guest
        setTransitionLeft(4, 5);
        //5_left to 8_right - guest to bath2
        setTransitionLeft(5, 8);
        //8_left to 4_right - bath2 to living
        setTransitionLeft(8, 4);

        //deactivate the selected doors
        //0_left - lily left
        setTransitionLeftActive(0, false);
        //9_left - hallway left
        setTransitionLeftActive(9, false);
        //10_left - brother left
        setTransitionLeftActive(10, false);
        //10_right - brother right
        setTransitionRightActive(10, false);
    }
}
